import os
import time
import statistics


def cls():
    os.system('cls' if os.name=='nt' else 'clear')
    

class Timing:
    """
    timing = Timing(n_historie=20)
    while True:
        timing.start('First Thing', autostop=True)
        do_the_first_thing()

        timing.start('Second Thing', autostop=True)
        do_the_second_thing()

        timing.print_intervall(250) //in ms
    """
    def __init__(self, n_historie:int=10) -> None:
        self.n_historie = n_historie

        self.timing = {}
        self.print_time = time.time()
        self.counter = 0

        self.max_str = 0

        self.autostop_id = None


    def start(self, id:str, n_hist:int=None, autostop=False):
        if self.autostop_id is not None:
            self.stop(self.autostop_id)
            self.autostop_id = None

        try:
            one_time = self.timing[id]
        except:
            if n_hist is None:
                n_hist = self.n_historie
            self.timing.update({id: {'hist': [], 'start': 0, 'position': 0, 'n_hist': n_hist}})
            length = len(id) + 1
            if length > self.max_str:
                self.max_str = length
            one_time = self.timing[id]

        one_time['start'] = time.time()
        if autostop:
            self.autostop_id = id


    def stop(self, id:str):
        one_time = self.timing[id]
        diff = time.time() - one_time['start']

        try:
            one_time['hist'][one_time['position']] = diff
        except:
            one_time['hist'].append(diff)

        one_time['position'] = (one_time['position'] + 1) % one_time['n_hist']


    def print_intervall(self, intervall:int):
        self.counter += 1
        if self.print_time + intervall/1000  <= time.time() and self.counter >= self.n_historie:
            self.counter = self.n_historie
            result_str = self.__str__()
            cls()
            print(result_str)
            self.print_time = time.time()


    def __str__(self) -> str:
        text = ""
        sum_of_mean = []
        for id, one_time in self.timing.items():
            mean = statistics.mean(one_time['hist']) * 1000
            sd = statistics.stdev(one_time['hist']) * 1000
            str_id = id + ':'
            text += "{id:{leng}} {mean:8.2f}ms ({sd:6.2f})\n".format(id=str_id, leng=self.max_str, mean=mean, sd=sd)

            sum_of_mean.append(mean)

        sum_of_mean = sum(sum_of_mean)
        fps = 1000 / sum_of_mean
        text += "{id:{leng}} {mean:8.2f}ms ({fps:6.2f}fps)\n".format(id='Summe', leng=self.max_str, mean=sum_of_mean, fps=fps)

        return text

